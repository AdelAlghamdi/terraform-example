from django.shortcuts import render
from django.views import View

# Create your views here.
class mainView(View):
    template = 'index.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template)