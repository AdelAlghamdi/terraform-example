# Fetch AWS managed policy
data "aws_iam_policy" "beanstalk_web_tier_policy"{
    arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}
data "aws_iam_policy" "beanstalk_multi_conatiner_policy"{
    arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
}
data "aws_iam_policy" "beanstalk_worker_tier_policy"{
    arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
}
# EC2 Beanstalk role ----------------------------
resource "aws_iam_role" "beanstalk_ec2_role"{
    name = format("%s-%s-beanstalk-ec2-role", var.app_name, var.purpose_tag)
    # attach with AWS managed policies
    managed_policy_arns = [
        data.aws_iam_policy.beanstalk_web_tier_policy.arn,
        data.aws_iam_policy.beanstalk_multi_conatiner_policy.arn,
        data.aws_iam_policy.beanstalk_worker_tier_policy.arn
    ]
    # associate as trusted entity with EC2 (allowing it to be associated with EC2)
    assume_role_policy = jsonencode(
        {
            "Version": "2008-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "ec2.amazonaws.com"
                    },
                    "Action": "sts:AssumeRole"
                }
            ]
        }
    )
}
# EC2 Beanstalk role ----------------------------
# Beanstalk EC2 profile role ---------------------
resource "aws_iam_instance_profile" "beanstalk_ec2_profile_role"{
    name = format("%s-%s-beanstalk-ec2-profile-role", var.app_name, var.purpose_tag)
    role = aws_iam_role.beanstalk_ec2_role.name
}

resource "aws_iam_role" "beanstalk_service_role"{
    name = format("%s-%s-beanstalk-service-role", var.app_name, var.purpose_tag)
    assume_role_policy = jsonencode(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "elasticbeanstalk.amazonaws.com"
                    },
                    "Action": "sts:AssumeRole",
                    "Condition": {
                        "StringEquals": {
                            "sts:ExternalId": "elasticbeanstalk"
                        }
                    }
                }
            ]
        }
    )
}
# Beanstalk role ------------------------------
# Beanstalk role Policy ------------------------------
resource "aws_iam_role_policy" "beanstalk_service_role_policy" {
    name = format("%s-%s-beanstalk-service-policy", var.app_name, var.purpose_tag)
    role = aws_iam_role.beanstalk_service_role.id

    # policy statements allowing beanstalk to access AWS resources in order to function as expected
    policy = jsonencode (
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "VisualEditor0",
                    "Effect": "Allow",
                    "Action": [
                        "ec2:AuthorizeSecurityGroupIngress",
                        "ec2:DescribeInstances",
                        "elasticloadbalancing:RegisterTargets",
                        "ec2:DescribeSnapshots",
                        "codebuild:BatchGetBuilds",
                        "autoscaling:PutScheduledUpdateGroupAction",
                        "elasticloadbalancing:DeleteLoadBalancer",
                        "s3:GetObjectAcl",
                        "elasticloadbalancing:DescribeLoadBalancers",
                        "ec2:RevokeSecurityGroupEgress",
                        "autoscaling:DescribeAutoScalingGroups",
                        "ecs:RegisterTaskDefinition",
                        "ec2:DescribeAccountAttributes",
                        "autoscaling:UpdateAutoScalingGroup",
                        "sns:Subscribe",
                        "autoscaling:DescribeNotificationConfigurations",
                        "ec2:DescribeKeyPairs",
                        "elasticloadbalancing:DescribeInstanceHealth",
                        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
                        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                        "autoscaling:TerminateInstanceInAutoScalingGroup",
                        "autoscaling:PutNotificationConfiguration",
                        "sqs:GetQueueUrl",
                        "sns:ListSubscriptionsByTopic",
                        "autoscaling:ResumeProcesses",
                        "ec2:DescribeLaunchTemplates",
                        "autoscaling:SetDesiredCapacity",
                        "ecs:CreateCluster",
                        "sns:CreateTopic",
                        "ec2:RunInstances",
                        "ecs:DeleteCluster",
                        "elasticloadbalancing:DeregisterTargets",
                        "autoscaling:SuspendProcesses",
                        "sqs:GetQueueAttributes",
                        "autoscaling:DescribeLoadBalancers",
                        "logs:CreateLogGroup",
                        "ec2:DescribeVpcClassicLink",
                        "codebuild:CreateProject",
                        "ecs:DescribeClusters",
                        "ec2:RevokeSecurityGroupIngress",
                        "s3:GetObject",
                        "autoscaling:AttachInstances",
                        "rds:DescribeOrderableDBInstanceOptions",
                        "autoscaling:DeleteAutoScalingGroup",
                        "codebuild:StartBuild",
                        "ec2:AssociateAddress",
                        "ec2:DescribeSubnets",
                        "autoscaling:CreateAutoScalingGroup",
                        "autoscaling:DetachInstances",
                        "autoscaling:DeleteScheduledAction",
                        "ec2:DisassociateAddress",
                        "autoscaling:DescribeAutoScalingInstances",
                        "ec2:DescribeAddresses",
                        "rds:DescribeDBEngineVersions",
                        "ec2:DescribeInstanceAttribute",
                        "elasticloadbalancing:ConfigureHealthCheck",
                        "autoscaling:DescribeLaunchConfigurations",
                        "s3:ListBucket",
                        "sns:SetTopicAttributes",
                        "ec2:DescribeSpotInstanceRequests",
                        "autoscaling:DeletePolicy",
                        "iam:PassRole",
                        "autoscaling:DescribeScalingActivities",
                        "ec2:CreateSecurityGroup",
                        "autoscaling:DescribeAccountLimits",
                        "autoscaling:PutScalingPolicy",
                        "rds:DescribeDBInstances",
                        "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
                        "autoscaling:DescribeScheduledActions",
                        "ec2:DeleteLaunchTemplateVersions",
                        "ec2:ReleaseAddress",
                        "ec2:AuthorizeSecurityGroupEgress",
                        "ec2:DeleteLaunchTemplate",
                        "ec2:TerminateInstances",
                        "elasticloadbalancing:CreateLoadBalancer",
                        "logs:DescribeLogGroups",
                        "sns:GetTopicAttributes",
                        "ec2:DescribeLaunchTemplateVersions",
                        "iam:ListRoles",
                        "ec2:AllocateAddress",
                        "ec2:DescribeSecurityGroups",
                        "ec2:CreateLaunchTemplateVersion",
                        "ec2:DescribeImages",
                        "autoscaling:CreateLaunchConfiguration",
                        "cloudwatch:PutMetricAlarm",
                        "elasticbeanstalk:*",
                        "ec2:CreateLaunchTemplate",
                        "autoscaling:DeleteLaunchConfiguration",
                        "ec2:DescribeVpcs",
                        "ec2:DeleteSecurityGroup",
                        "elasticloadbalancing:DescribeTargetHealth",
                        "elasticloadbalancing:DescribeTargetGroups",
                        "codebuild:DeleteProject",
                        "logs:PutRetentionPolicy"
                    ],
                "Resource": "*"
                },
                {
                    "Sid": "VisualEditor1",
                    "Effect": "Allow",
                    "Action": "logs:DeleteLogGroup",
                    "Resource": "arn:aws:logs:*:*:log-group:/aws/elasticbeanstalk*"
                },
                {
                    "Sid": "VisualEditor2",
                    "Effect": "Allow",
                    "Action": "cloudformation:*",
                    "Resource": [
                        "arn:aws:cloudformation:*:*:stack/awseb-*",
                        "arn:aws:cloudformation:*:*:stack/eb-*"
                    ]
                },
                {
                    "Sid": "VisualEditor3",
                    "Effect": "Allow",
                    "Action": "s3:*",
                    "Resource": [
                        "arn:aws:s3:::elasticbeanstalk-*/*",
                        "arn:aws:s3:::elasticbeanstalk-*"
                    ]
                }
            ]
        }
    )
}
# Beanstalk role Policy ------------------------------