resource "random_string" "s3_unique_name" {
    length = 5
    lower = true
    upper = false
    special = false
}

resource "aws_s3_bucket" "app_s3" {
    # must be same as resource in below policy. Otherwise, S3 might be created with broken policy
    bucket = format("%s-%s-%s", var.app_name, var.purpose_tag, random_string.s3_unique_name.result)
    # set force to "false" on production environment
    force_destroy = true
    policy = jsonencode(
        {
            "Version": "2012-10-17",
            "Id": "http referer policy example",
            "Statement": [
                {
                    "Sid": "Allow get requests originating from www.example.com and example.com.",
                    "Effect": "Allow",
                    "Principal": "*",
                    "Action": [
                        "s3:PutObject",
                        "s3:GetObject",
                        "s3:GetObjectVersion"
                    ],
                    "Resource": [
                        # make sure second parameter is same is bucket name (format("%s-%s-%s", var.app_name, var.purpose_tag, random_string.s3_unique_name.result))
                        format("arn:aws:s3:::%s/static/*", format("%s-%s-%s", var.app_name, var.purpose_tag, random_string.s3_unique_name.result)),
                        format("arn:aws:s3:::%s/static", format("%s-%s-%s", var.app_name, var.purpose_tag, random_string.s3_unique_name.result))
                    ]
                }
            ]
        }
    )
}

output "s3_name" {
    description = "S3 app bucket ARN"
    value       = aws_s3_bucket.app_s3.arn
}