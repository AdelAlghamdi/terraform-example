resource "aws_security_group" "ec2_security_group" {
    name = format("TCP-open-%s-%s", var.app_name, var.purpose_tag)
    description = "Allow TLS inbound traffic"
    vpc_id = aws_vpc.app_vpc.id

    ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = ["::/0"]
    }
    ingress {
    description      = "Allow SSH access to EC2"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = ["::/0"]
    }
    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
    Name = format("TCP-open-%s-%s", var.app_name, var.purpose_tag)
    }
}
resource "aws_security_group" "rds_security_group" {
    name = format("RDS-%s-%s", var.app_name, var.purpose_tag)
    description = "Allow postgres inbound traffic"
    vpc_id = aws_vpc.app_vpc.id

    ingress {
    description      = "TLS from VPC"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    }
    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
    Name = format("RDS-%s-%s", var.app_name, var.purpose_tag)
    }
}