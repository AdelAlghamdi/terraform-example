resource "aws_key_pair" "terraform" {
    key_name   = format("%s-%s", var.app_name, var.purpose_tag)
    public_key = ""
}