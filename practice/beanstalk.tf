resource "aws_elastic_beanstalk_application" "platform" {
    name = format("%s-%s-app", var.app_name, var.purpose_tag)
    description = format("Platform to host %s app", var.app_name)
    appversion_lifecycle {
        service_role = aws_iam_role.beanstalk_service_role.arn
        max_age_in_days = 365
        # Prevent deleting versions when platform is detroyed
        delete_source_from_s3 = false
    }
    tags = {
        Name = format("%s-%s", var.app_name, var.purpose_tag)
        Platform = var.technology
    }
}

resource "aws_elastic_beanstalk_configuration_template" "beanstalk_template" {
    name = "laravel-template-config"
    application = aws_elastic_beanstalk_application.platform.name
    solution_stack_name = var.platform
    # https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options.html
    setting {
        namespace = "aws:ec2:vpc"
        name = "VPCId"
        value = aws_vpc.app_vpc.id
    }
    setting {
        namespace = "aws:ec2:vpc"
        name = "Subnets"
        value = join (",", [aws_subnet.public1.id, aws_subnet.public2.id, aws_subnet.public3.id])
    }
    setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name = "EC2KeyName"
        value = aws_key_pair.terraform.key_name
    }
    setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name = "InstanceType"
        value = var.ec2_family
    }
    setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name = "IamInstanceProfile"
        value = aws_iam_instance_profile.beanstalk_ec2_profile_role.arn
    }
    setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name = "ImageId"
        value = var.image_id
    }
    setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name = "SecurityGroups"
        value = aws_security_group.ec2_security_group.id
    }
    setting {
        namespace = "aws:elb:loadbalancer"
        name = "CrossZone"
        value = true
    }
    setting {
        namespace = "aws:ec2:instances"
        name = "EnableSpot"
        value = true
    }
    setting {
        namespace = "aws:autoscaling:asg"
        name = "MinSize"
        value = var.min_ec2s
    }
    setting {
        namespace = "aws:autoscaling:asg"
        name = "MaxSize"
        value = var.max_ec2s
    }
    setting {
        namespace = "aws:elasticbeanstalk:environment"
        name = "ServiceRole"
        value = aws_iam_role.beanstalk_service_role.arn
    }
    setting {
        namespace = "aws:elasticbeanstalk:environment"
        name = "EnvironmentType"
        value = "LoadBalanced"
    }
    setting {
        namespace = "aws:elasticbeanstalk:environment"
        name = "LoadBalancerType"
        value = "application"
    }
    setting {
        namespace = "aws:elb:healthcheck"
        name = "HealthyThreshold"
        value = 3
    }
    setting {
        namespace = "aws:elb:healthcheck"
        name = "Interval"
        value = 10
    }
    setting {
        namespace = "aws:elb:healthcheck"
        name = "UnhealthyThreshold"
        value = 5
    }
    setting {
        namespace = "aws:elb:listener"
        name = "ListenerProtocol"
        value = "HTTP"
    }
    setting {
        namespace = "aws:elb:listener"
        name = "InstancePort"
        value = "80"
    }
}

resource "aws_elastic_beanstalk_environment" "application" {
    name = format("%s-%s-app", var.app_name, var.purpose_tag)
    application = aws_elastic_beanstalk_application.platform.name
    cname_prefix = format("%s-%s-env", var.app_name, var.purpose_tag)
    tier = "WebServer"
    template_name = aws_elastic_beanstalk_configuration_template.beanstalk_template.name
}

output "beanstalk_host" {
    description = "Web host URL"
    value       = aws_elastic_beanstalk_environment.application.cname
}
output "beanstalk_app_name"{
    description = "Beanstalk Application name"
    value = aws_elastic_beanstalk_application.platform.name
}
output "beanstalk_environment_name" {
    description = "Beanstalk Environment Name"
    value = aws_elastic_beanstalk_environment.application.name
}
