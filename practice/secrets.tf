resource "random_password" "master_password" {
    length = 16
    special = false
}
# RDS credentials ----------------------------
resource "aws_secretsmanager_secret" "rds_credentials" {
    name = format("rds-cred-%s-%s", var.app_name, var.purpose_tag)
    recovery_window_in_days = 0
}
resource "aws_secretsmanager_secret_version" "rds_credentials" {
    secret_id     = aws_secretsmanager_secret.rds_credentials.id
    secret_string = jsonencode(
        {
            "db_name": aws_db_instance.app_rds.name
            "username": aws_db_instance.app_rds.username,
            "password": random_password.master_password.result,
            "engine": aws_db_instance.app_rds.engine,
            "host": aws_db_instance.app_rds.address,
            "endpoint": aws_db_instance.app_rds.endpoint,
            "port": aws_db_instance.app_rds.port,
            "identifier": aws_db_instance.app_rds.identifier
        }
    )
}
# RDS credentials ----------------------------
# app user credentials ----------------------------
resource "aws_secretsmanager_secret" "app_user" {
    name = format("app-user-cred-%s-%s", var.app_name, var.purpose_tag)
    recovery_window_in_days = 0
}
resource "aws_secretsmanager_secret_version" "app_user_version" {
    secret_id     = aws_secretsmanager_secret.app_user.id
    secret_string = jsonencode(
        {
            "username": aws_iam_user.app_user.name
            "username_arn": aws_iam_user.app_user.arn
            "key": aws_iam_access_key.app_user.id
            "secret_key": aws_iam_access_key.app_user.secret
        }
    )
}
# app user credentials ----------------------------
# gitlab user credentials ----------------------------
resource "aws_secretsmanager_secret" "gitlab_user" {
    name = format("app-gitlab-user-cred-%s-%s", var.app_name, var.purpose_tag)
    recovery_window_in_days = 0
}
resource "aws_secretsmanager_secret_version" "gitlab_user_version" {
    secret_id     = aws_secretsmanager_secret.gitlab_user.id
    secret_string = jsonencode(
        {
            "username": aws_iam_user.gitlab_user.name
            "username_arn": aws_iam_user.gitlab_user.arn
            "key": aws_iam_access_key.gitlab_user.id
            "secret_key": aws_iam_access_key.gitlab_user.secret
        }
    )
}
# gitlab user credentials ----------------------------