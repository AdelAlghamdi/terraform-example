# Fetching managed AWS policy
data "aws_iam_policy" "cloudwatch_policy" {
    arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}
# create application user ------------------------------
resource "aws_iam_user" "app_user" {
    name = format("app-%s-%s", var.app_name, var.purpose_tag)
    force_destroy = true
}
resource "aws_iam_access_key" "app_user" {
    user = aws_iam_user.app_user.name
    status = "Active"
}
resource "aws_iam_policy" "s3_policy" {
    name = format("app-s3-%s-%s", var.app_name, var.purpose_tag)
    policy = jsonencode(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": [
                        "s3:*",
                        "s3-object-lambda:*"
                    ],
                    "Resource": [
                        format("arn:aws:s3:::%s", aws_s3_bucket.app_s3.bucket),
                        format("arn:aws:s3:::%s/*", aws_s3_bucket.app_s3.bucket)
                    ]
                },
                {
                    "Effect": "Allow",
                    "Action": [
                        "s3:ListAllMyBuckets",
                        "s3:GetBucketLocation"
                    ],
                    "Resource": "*"
                }
            ]
        }
    )
}
resource "aws_iam_policy" "app_user_secret" {
    name = format("app-secret-%s-%s", var.app_name, var.purpose_tag)
    policy = jsonencode(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": [
                        "secretsmanager:*",
                        "cloudformation:CreateChangeSet",
                        "cloudformation:DescribeChangeSet",
                        "cloudformation:DescribeStackResource",
                        "cloudformation:DescribeStacks",
                        "cloudformation:ExecuteChangeSet",
                        "ec2:DescribeSecurityGroups",
                        "ec2:DescribeSubnets",
                        "ec2:DescribeVpcs",
                        "kms:DescribeKey",
                        "kms:ListAliases",
                        "kms:ListKeys",
                        "lambda:ListFunctions",
                        "rds:DescribeDBClusters",
                        "rds:DescribeDBInstances",
                        "redshift:DescribeClusters",
                        "tag:GetResources"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        aws_secretsmanager_secret.app_user.arn,
                        aws_secretsmanager_secret.rds_credentials.arn
                    ]
                },
                {
                    "Action": [
                        "lambda:AddPermission",
                        "lambda:CreateFunction",
                        "lambda:GetFunction",
                        "lambda:InvokeFunction",
                        "lambda:UpdateFunctionConfiguration"
                    ],
                    "Effect": "Allow",
                    "Resource": "arn:aws:lambda:*:*:function:SecretsManager*"
                },
                {
                    "Action": [
                        "serverlessrepo:CreateCloudFormationChangeSet",
                        "serverlessrepo:GetApplication"
                    ],
                    "Effect": "Allow",
                    "Resource": "arn:aws:serverlessrepo:*:*:applications/SecretsManager*"
                },
                {
                    "Action": [
                        "s3:GetObject"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "arn:aws:s3:::awsserverlessrepo-changesets*",
                        "arn:aws:s3:::secrets-manager-rotation-apps-*/*"
                    ]
                }
            ]
        }
    )
}
resource "aws_iam_user_policy_attachment" "secret_policy_association" {
    user = aws_iam_user.app_user.name
    policy_arn = aws_iam_policy.app_user_secret.arn
}
resource "aws_iam_user_policy_attachment" "s3_policy_association" {
    user = aws_iam_user.app_user.name
    policy_arn = aws_iam_policy.s3_policy.arn
}
resource "aws_iam_user_policy_attachment" "cloudwatch_policy_association" {
    user = aws_iam_user.app_user.name
    policy_arn = data.aws_iam_policy.cloudwatch_policy.arn
}
# create application user ------------------------------
# create GitLab user -----------------------------
data "aws_iam_policy" "beanstalk_policy" {
    arn = "arn:aws:iam::aws:policy/AdministratorAccess-AWSElasticBeanstalk"
}
resource "aws_iam_policy" "beanstalk_s3_gitlab_policy" {
    name = format("app-s3-gitlab-%s-%s", var.app_name, var.purpose_tag)
    policy = jsonencode(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                "Action": [
                    "s3:*",
                    "s3-object-lambda:*"
                ],
                "Resource": [
                    "arn:aws:s3:::elasticbeanstalk*"
                ]
                }
            ]
        }
    )
}
resource "aws_iam_user" "gitlab_user" {
    name = format("app-gitlab-%s-%s", var.app_name, var.purpose_tag)
    force_destroy = true
}
resource "aws_iam_access_key" "gitlab_user" {
    user = aws_iam_user.gitlab_user.name
}
resource "aws_iam_user_policy_attachment" "gitlab_beanstalk" {
    user = aws_iam_user.gitlab_user.name
    policy_arn = data.aws_iam_policy.beanstalk_policy.arn
}
resource "aws_iam_user_policy_attachment" "gitlab_beanstalk_s3" {
    user = aws_iam_user.gitlab_user.name
    policy_arn = aws_iam_policy.beanstalk_s3_gitlab_policy.arn
}
# create GitLab user -----------------------------