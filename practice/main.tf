terraform {
    required_providers {
    aws = {
        source  = "hashicorp/aws"
        version = "~> 3.27"
        }
    }
    required_version = ">= 0.14.8"
}

provider "aws" {
    profile    = "default"
    access_key = var.aws_access_key
    secret_key = var.aws_secret
    region     = var.aws_region

    default_tags {
        tags = {
            Environment = var.purpose_tag
            "Cost Center" = var.purpose_tag
        }
    }
}