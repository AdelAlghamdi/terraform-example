resource "aws_db_instance" "app_rds" {
    allocated_storage = 20
    engine = "postgres"
    apply_immediately = true
    backup_window = "11:00-11:30"
    copy_tags_to_snapshot = true
    deletion_protection = true
    identifier = format("%s-%s", var.app_name, var.purpose_tag)
    instance_class = "db.t3.micro"
    maintenance_window = "sun:12:00-sun:12:30"
    max_allocated_storage = 50
    name = var.db_name
    username = format("user_%s_%s", var.app_name, var.purpose_tag)
    password = random_password.master_password.result
    multi_az = false
    backup_retention_period = 35
    storage_type = "gp2"
    publicly_accessible = true
    skip_final_snapshot = true
    vpc_security_group_ids = [aws_security_group.rds_security_group.id]
    db_subnet_group_name = aws_db_subnet_group.rds_subnet.name
    tags = {
        Name = format("%s-%s", var.app_name, var.purpose_tag)
    }
    depends_on = [aws_security_group.rds_security_group]
}

output "rds_host" {
    description = "RDS host endpoint"
    value       = aws_db_instance.app_rds.address
}