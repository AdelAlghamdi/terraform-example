variable "app_name" {
    type = string
    default = "pure"
    description = "Please name your infrastructure project"
}
variable "purpose_tag" {
    type = string
    description = "purpose of your project, allowed values are [spot, dev, uat, prod]."
    default = "dev"
    validation {
        condition = contains(["spot", "dev", "uat", "prod"], var.purpose_tag)
        error_message = "Please write one of the following values [spot, dev, uat, prod]."
    }
}

