variable "aws_access_key" {
    type = string
    # default = ""
    description = "AWS access key"
}
variable "aws_secret" {
    type = string
    # default = ""
    description = "AWS secret key"
}
variable "aws_region" {
    type = string
    default = "eu-west-2"
    description = "aws default region"
}
variable "db_name" {
    type = string
    default = "yasser"
    description = "Postgres RDS physical database name"
}
variable "technology" {
    # Only use 1 default value and comment the others. This must not conflict with "platform" variable
    type = string
    description = "Choose on of the following platforms [Docker, Python, PHP, Node.js, Java SE]"
    # default = "Docker"
    default = "Python"
    # default = "PHP"
    # default = "Node.js"
    # default = "Java SE"
    validation {
        condition = contains(["Docker", "Python", "PHP", "Node.js", "Java SE"], var.technology)
        error_message = "Please write one of the following values [Docker, Python, PHP, Node.js, Java SE]."
    }
}
variable "platform" {
    # https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported
    type = string
    description = "Please type correct platform supported by beanstalk. find it in AWS docs https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported"
    # Docker
    # default = "64bit Amazon Linux 2 v3.4.9 running Docker"

    # Python
    default = "64bit Amazon Linux 2 v3.3.8 running Python 3.8"

    # PHP
    # default = "64bit Amazon Linux 2 v3.3.8 running PHP 8.0"

    # Java SE
    # default = "64bit Amazon Linux 2 v3.2.8 running Corretto 11"

    # Node.js
    # default = "64bit Amazon Linux 2 v5.4.8 running Node.js 14"
}
variable "ec2_family" {
    type = string
    description = "EC2 family such as t2.micro, t3.small"
    default = "t3a.micro"
    validation {
        condition = contains(["t2.micro", "t3.micro", "t3a.micro", "t3a.small", "t3a.medium"], var.ec2_family)
        error_message = "Please correct EC2 family to be one of the following [t2.micro, t3.micro, t3a.micro, t3a.medium]."
    }
}
variable "min_ec2s" {
    type = string
    default = "1"
    description = "Minimum number of EC2 in ASG"
}
variable  "max_ec2s" {
    type = string
    default = "4"
    description = "Minimum number of EC2 in ASG"
}
variable "image_id" {
    type = string
    default = "ami-0c0a1cc13a52a158f"
    description = "Default image to be used by beanstalk (Linux 2). Differ from region to another"
}