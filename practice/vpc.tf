# VPC -----------------------------------
resource "aws_vpc" "app_vpc" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"
    enable_dns_support = true
    enable_dns_hostnames = true

    tags = {
        Name = format("%s-vpc", var.app_name)
    }
}
# Subnet -----------------------------------
resource "aws_subnet" "public1" {
    vpc_id = aws_vpc.app_vpc.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = true
    availability_zone = format("%sa", var.aws_region)
    tags = {
        Name = "Public subnet1"
    }
}
resource "aws_subnet" "public2" {
    vpc_id = aws_vpc.app_vpc.id
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = true
    availability_zone = format("%sb", var.aws_region)
    tags = {
        Name = "Public subnet2"
    }
}
resource "aws_subnet" "public3" {
    vpc_id = aws_vpc.app_vpc.id
    cidr_block = "10.0.3.0/24"
    map_public_ip_on_launch = true
    availability_zone = format("%sc", var.aws_region)
    tags = {
        Name = "Public subnet3"
    }
}
resource "aws_subnet" "private4" {
    vpc_id = aws_vpc.app_vpc.id
    cidr_block = "10.0.4.0/24"
    map_public_ip_on_launch = true
    availability_zone = format("%sc", var.aws_region)
    tags = {
        Name = "Private subnet4"
    }
}
resource "aws_subnet" "private5" {
    vpc_id = aws_vpc.app_vpc.id
    cidr_block = "10.0.5.0/24"
    map_public_ip_on_launch = true
    availability_zone = format("%sb", var.aws_region)
    tags = {
        Name = "Private subnet5"
    }
}
resource "aws_db_subnet_group" "rds_subnet"{
    name = "db-subnet"
    subnet_ids = [aws_subnet.private4.id, aws_subnet.private5.id]
    tags = {
        Name = "Private RDS subnet4"
    }
}
# Subnet -----------------------------------
# Internet Gateway -----------------------------------
resource "aws_internet_gateway" "default" {
    vpc_id = aws_vpc.app_vpc.id
    depends_on = [aws_vpc.app_vpc]
    tags = {
        Name = format("IG-%s", var.app_name)
    }
}
# Internet Gateway -----------------------------------
# Route Table -----------------------------------
resource "aws_route_table" "app_routes"{
    vpc_id = aws_vpc.app_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.default.id
    }
    tags = {
        Name = format("RT-%s", var.app_name)
    }
}
# resource "aws_main_route_table_association" "default"{
#     vpc_id = aws_vpc.app_vpc.id
#     route_table_id = aws_route_table.app_routes.id
#     depends_on = [aws_route_table.app_routes]
# }
# Linking Subnets --------------------------------------
resource "aws_route_table_association" "rt-subnet1" {
    route_table_id = aws_route_table.app_routes.id
    subnet_id = aws_subnet.public1.id
}
resource "aws_route_table_association" "rt-subnet2" {
    route_table_id = aws_route_table.app_routes.id
    subnet_id = aws_subnet.public2.id
}
resource "aws_route_table_association" "rt-subnet3" {
    route_table_id = aws_route_table.app_routes.id
    subnet_id = aws_subnet.public3.id
}
# Linking Subnets --------------------------------------
# Route Table -----------------------------------
# ----------------------------------- Output